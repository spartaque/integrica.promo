<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::group([
//    'middleware' => ['url.locale'],
//], function () {
//    Route::locales(function () {
//        Route::group(['domain' => 'promo.' . config('env.APP_DOMAIN')], function () {
//            Route::view('', 'index')->name('promo.index');
//        });

//        Route::group(['domain' => config('env.APP_DOMAIN')], function () {
            Route::view('', 'index.index')->name('index');
//            Route::view('contacts', 'contacts')->name('contacts');
//            Route::view('offer', 'offer.index')->name('offer');
//            Route::view('license', 'license.index')->name('license');
//            Route::post('registration', 'Auth\RegisterController@register');
//            Route::post('recoveryPasswordSend', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//            Route::post('resetPassword', 'Auth\ResetPasswordController@reset');
//        });
//
//        Route::group(['domain' => 'g.' . config('env.APP_DOMAIN')], function () {
//            Route::view('', 'index')->name('g.index');
//        });
//    });
//});
