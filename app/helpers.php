<?php
declare(strict_types=1);

use Illuminate\Support\Str;

if (!function_exists('sendApiRequest')) {

    /**
     * Отправка HTTP запроса к API
     *
     * @param $method
     * @param $api_method
     * @param $params
     * @param $debug
     * @return array
     */
    function sendApiRequest($method, string $api_method, $params = [], $debug = false): array
    {
        $http = new \GuzzleHttp\Client([]);

        $jar = new \GuzzleHttp\Cookie\CookieJar;
        $session = config('session.cookie');

        $jar->setCookie(new \GuzzleHttp\Cookie\SetCookie([
            'Name' => $session,
            'Value' => $_COOKIE[$session] ?? '', // Use this way to send not encrypted cookie value
            'Domain' => \request()->server('HTTP_HOST'),
            'Path' => config('session.path'),
        ]));

        try {
            $uri = getSelfHost() . "/{$api_method}";

            $options = [
                'verify' => false,
                'form_params' => $params,
                'allow_redirects' => false,
                'cookies' => $jar,
            ];

            if ($method === 'GET') {
                $uri .= '?' . http_build_query($params);
            } else {
                $options['form_params'] = $params;
            }

            $response = $http->request($method, $uri, $options);

            $response = json_decode((string)$response->getBody(), true);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $response = json_decode((string)$e->getResponse()->getBody(), true);

            if (isset($response['status_code'])) {
                switch ($response['status_code']) {
                    case 401:
                        throw new App\Exceptions\BadAuth();

                    case 403:
                        throw new App\Exceptions\ForeignCabinet();

                    case 500:
                        throw new App\Exceptions\ApiError($response['message']);
                        break;
                }
            }
        }

        if ($debug) {
            dd($response);
        }

        return $response;
    }
}

if (!function_exists('getIp')) {
    function getIp(): string
    {
        if (app()->isLocal()) {
            return '212.90.59.69';
        }

        return (new \App\Services\IpInspector())->getIp();
    }
}

if (!function_exists('getRandomCode')) {
    /**
     * Получение рандомной строки заданой длины
     *
     * @param int $length
     * @param null $alphabet
     * @return string
     */
    function getRandomCode(int $length = 6, $alphabet = null)
    {
        $length = (int)$length;

        if (empty($alphabet)) {
            $alphabet = [
                'a', 'b', 'c',
                'd', 'e', 'f',
                'g', 'h', 'j',
                'k', 'm', 'n',
                'p', 'q', 'r',
                's', 't', 'u',
                'v', 'w', 'x',
                'y', 'z', '1',
                '2', '3', '4',
                '5', '6', '7',
                '8', '9', '0',
                'A', 'B', 'C',
                'D', 'E', 'F',
                'G', 'H', 'J',
                'K', 'M', 'N',
                'P', 'Q', 'R',
                'S', 'T', 'U',
                'V', 'W', 'X',
                'Y', 'Z',
            ];
            shuffle($alphabet);
        }

        $code = '';
        $max_index = count($alphabet) - 1;

        for ($i = 0; $i < $length; $i++) {
            $rand_let = rand(0, $max_index);
            $code .= $alphabet[$rand_let];
        }

        return $code;
    }
}

if (!function_exists('getIdFromHash')) {
    function getIdFromHash($hash)
    {
        if (is_string($hash)) {
            return \Hashids::decode($hash)[0] ?? 0;
        }

        return 0;
    }
}


if (!function_exists('getIdsByHashes')) {
    function getIdsByHashes(array $hashes = [])
    {
        if (!count($hashes)) {
            return [];
        }

        return collect(
            array_map(function ($hash) {
                return \Hashids::decode($hash)[0] ?? null;
            }, $hashes)
        )
            ->reject(function ($value) {
                return is_null($value);
            })->toArray();
    }
}

if (!function_exists('isPostmanRequest')) {
    function isPostmanRequest()
    {
        return Str::contains(request()->userAgent(), 'PostmanRuntime');
    }
}

if (!function_exists('isApiRequest')) {
    function isApiRequest()
    {
        // If bad HTTP method exception
        if (\is_null(app('router')->current())) {
            return false;
        }

        return Str::startsWith(app('router')->current()->getAction()['prefix'], 'api');
    }
}

if (!function_exists('rejectEmpty')) {
    function rejectEmpty(array $array)
    {
        return collect($array)
            ->reject(function ($item) {
                return empty($item);
            })
            ->toArray();
    }
}

if (!function_exists('dbd')) {
    /**
     * Showing all database queries.
     *
     * @param null|\Illuminate\Console\Command|\Psr\Log\LoggerInterface $channel
     */
    function dbd($channel = null)
    {
        static $initialized;

        if ($initialized) {
            return;
        }
        app('db')->listen(function ($sql) use ($channel) {
            foreach ($sql->bindings as $i => $binding) {
                $sql->bindings[$i] = is_string($binding) ? "'$binding'" : (string)$binding;
            }

            $time = $sql->time / 1000;
            $query = "[{$time}s] ";
            $query .= str_replace(['%', '?'], ['%%', '%s'], $sql->sql);
            $query = vsprintf($query, $sql->bindings);

            if (null === $channel) {
                dump($query);
            } elseif ($channel instanceof \Illuminate\Console\Command) {
                $channel->info($query);
            } elseif ($channel instanceof \Psr\Log\LoggerInterface) {
                $channel->info($query);
            }
        });
        $initialized = true;
    }
}

if (!function_exists('tzNameToOffset')) {
    function tzNameToOffset(string $tz)
    {
        return \Carbon\Carbon::now($tz)->getOffset() / 60;
    }
}

if (!function_exists('getSentryClient')) {
    function getSentryClient(): \Raven_Client
    {
        static $sentry_lient;

        if (!\is_null($sentry_lient)) {
            return $sentry_lient;
        }

        $user_config = app('sentry.config');

        $sentry_lient = \Sentry\SentryLaravel\SentryLaravel::getClient(array_merge([
            'environment' => app()->environment(),
            'prefixes' => [base_path()],
            'app_path' => app_path(),
        ], $user_config));

        return $sentry_lient;
    }
}


if (!function_exists('sendSentryMessage')) {
    function sendSentryMessage(string $message)
    {
        if (config('env.sentry_debug', false)) {
            getSentryClient()->capture([
                'message' => $message,
            ]);
        }
    }
}

if (!function_exists('paginationOffset')) {
    function paginationOffset($page, $per_page)
    {
        return ($page - 1) * $per_page;
    }
}

if (!function_exists('allEntitiesLoaded')) {
    function allEntitiesLoaded($total_entities, $page, $per_page)
    {
        return ($page * $per_page) >= $total_entities;
    }
}

if (!function_exists('generateRandomFilename')) {
    /**
     * Генерирует случайное имя файла и проверяет на существование такого в директории.
     * Есть 10 попыток сгенерировать уникальное имя файла в директории, после чего бросается исплючение
     *
     * @param $filepath
     * @param string $file_extension
     *
     * @return string
     */
    function generateRandomFilename($filepath, $file_extension = 'png'): string
    {
        $filename = '';
        $iteration = 0;

        while (true) {
            if ($iteration > 10) {
                throw new \LogicException('Too many attemps to generate unique filename.');
            }

            $filename = str_random() . '.' . $file_extension;

            if (!file_exists($filepath . $filename)) {
                break;
            }
            $iteration++;
        }

        return $filename;
    }
}

if (!function_exists('getSelfHost')) {
    function getSelfHost()
    {
        try {
            $domain = parse_url(url()->current());

            return $domain['scheme'] . '://' . $domain['host'];
        } catch (\Exception $e) {
            dd($e);
        }
    }
}

if (!function_exists('getTranslations')) {
    function getTranslations(array $files = [])
    {
        $locales = array_keys(config('localization.supported_locales'));
        $messages = [];

        foreach ($locales as $locale) {
            $messages[$locale] = [];
        }

        foreach ($files as $file) {
            foreach ($locales as $locale) {
                $messages[$locale][$file] = \Lang::get($file, [], $locale);
            }
        }

        return json_encode($messages);
    }
}

if (!function_exists('getDOBackupsFilepath')) {
    function getDOBackupsFilepath(string $filename)
    {
        return getDOBackupsPath() . '/' . $filename;
    }
}

if (!function_exists('getDOBackupsPath')) {
    function getDOBackupsPath()
    {
        return config('filesystems.disks.backups.bucket') . '/backups';
    }
}

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst(string $str)
    {
        return mb_convert_case($str, MB_CASE_TITLE, 'UTF-8');
    }
}

if (!function_exists('addInvalidParameterToValidator')) {
    function addInvalidParameterToValidator(
        \Illuminate\Contracts\Validation\Validator $validator,
        string $attribute = 'hash'
    ) {
        $validator->errors()->add('hash', trans('validation.in', [
            'attribute' => $attribute,
        ]));
    }
}

if (!function_exists('addErrorsToValidator')) {
    function addErrorsToValidator($failed_validator, $form_request)
    {
        foreach ($failed_validator->errors()->messages() as $error_field => $error) {
            $form_request->errors()->add($error_field, $error[0]);
        }
    }
}

if (!function_exists('convertToTranslit')) {
    function convertToTranslit($string)
    {
        $converter = [
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'і' => 'i', 'ї' => 'i\'', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
            'э' => 'e', 'є' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'І' => 'I', 'Ї' => 'I\'', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
            'Э' => 'E', 'Є' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        ];

        return strtr($string, $converter);
    }
}

if (!function_exists('isCyrillicEncoding')) {
    function isCyrillicEncoding(string $string): bool
    {
        return mb_detect_encoding($string) !== 'ASCII';
    }
}

if (!function_exists('isValidLiqpaySignature')) {
    function isValidLiqpaySignature(): bool
    {
        $request = \request()->all();
        $private_key = config('env.LIQPAY_PRIVATE_KEY');

        $signature = base64_encode(sha1($private_key . $request['data'] . $private_key));

        return $signature === $request['signature'];
    }
}

if (!function_exists('getDecodedLiqpayData')) {
    function getDecodedLiqpayData(): array
    {
        return json_decode(base64_decode(\request()->all()['data']), true);
    }
}

if (!function_exists('getLiqpayOrderId')) {
    function getLiqpayOrderId(): string
    {
        return getDecodedLiqpayData()['liqpay_order_id'];
    }
}

if (!function_exists('getSumInCents')) {
    function getSumInCents(float $sum): int
    {
        return (int)bcmul((string)$sum, '100');
    }
}

if (!function_exists('getAuthUser')) {
    function getAuthUser(): ?\App\Models\User
    {
        return Auth::user();
    }
}

if (!function_exists('getCompanyBranch')) {
    function getCompanyBranch(): ?\App\Models\CompanyBranch
    {
        return getAuthUser()->company_branch;
    }
}

if (!function_exists('microtime_float')) {
    function microtime_float()
    {
        [$usec, $sec] = explode(" ", microtime());

        return ((float)$usec + (float)$sec);
    }
}

if (!function_exists('getBackupFileDatetime')) {
    function getBackupFileDatetime(string $backupfile_prefix, string $filepath): int
    {
        $fileinfo = pathinfo($filepath);

        $datetime = str_replace($backupfile_prefix, '', $fileinfo['filename']);

        return (int)$datetime;
    }
}

if (!function_exists('getBrowserLocale')) {
    function getBrowserLocale(): string
    {
        $locale = mb_substr(request()->header('accept_language'), 0, 2);

        switch ($locale) {
            case "عر":
                return 'ar';

            default:
                return $locale;
        }
    }
}

if (!function_exists('getCleanValueOfMacro')) {
    function getCleanValueOfMacro(string $search, string $macro)
    {
        return trim(mb_substr($search, mb_strlen($macro . ':')));
    }
}

if (!function_exists('getFirstAndLastPartsOfName')) {
    function getFirstAndLastPartsOfName(string $full_name): array
    {
        $first_part = $full_name;
        $last_part = '';

        if (strpos($full_name, ' ') !== false) {
            [$first_part, $last_part] = explode(' ', $full_name, 2);

            if (isNotRealLastName($last_part)) {
                $last_part = '';
            }
        }

        return [
            'first_part' => $first_part,
            'last_part' => $last_part,
        ];
    }
}

if (!function_exists('isNotRealLastName')) {
    function isNotRealLastName(string $last_name): bool
    {
        return strlen($last_name) > 15 || preg_match('/[^\w+\s]/', $last_name);
    }
}

if (!function_exists('getPromoRoutes')) {
    function getPromoRoutes(): array
    {
        $locale = locale();
        $hide_locale = l10n()->shouldHideLocaleInUrl($locale);
        $prefix = $hide_locale ? '/' : '/' . $locale . '/';

        $routes = ['register', 'login', 'offer', 'license'];
        $response = [];

        foreach ($routes as $route) {
            $response[$route] = $prefix . $route;
        }

        $response['index'] = $prefix;
        $response['forgot_password'] = route($locale . '.password.request');

        return $response;
    }
}

if (!function_exists('isProduction')) {
    function isProduction(): bool
    {
        return app()->environment('production');
    }
}

if (!function_exists('unauthorized')) {
    function unauthorized()
    {
        abort(403, trans('auth.unauthorized'));

        return null;
    }
}

if (!function_exists('sendMessageToSuperadmin')) {
    function sendMessageToSuperadmin(string $message)
    {
        $job = new SendTelegramMessage();

        $job->setMessage($message);

        dispatch($job);
    }
}

if (!function_exists('sendZendeskReqeust')) {
    function sendZendeskReqeust(string $url)
    {
        $email = config('env.ZENDESK_EMAIL');
        $password = config('env.ZENDESK_PASSWORD');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $email . ':' . $password);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($ch);

        curl_close($ch);

        return $data;
    }
}

if (!function_exists('addLeadingHourZeroIfNeeded')) {
    function addLeadingHourZeroIfNeeded(int $hour)
    {
        return ($hour <= 9 ? '0' : '') . $hour;
    }
}

if (!function_exists('validateAvatar')) {
    function validateAvatar(
        \Illuminate\Http\Request $request,
        \Illuminate\Contracts\Validation\Validator $validator
    ) {
        if ($request->hasFile('avatar')) {
            $avatar_validator = \Validator::make(
                ['avatar' => $request->file('avatar')],
                ['avatar' => 'image|max:512']
            );

            if ($avatar_validator->fails()) {
                addErrorsToValidator($avatar_validator, $validator);
            }
        }
    }
}
if (!function_exists('table_joined')) {
    function table_joined($builder, string $table): bool
    {
        $joins = $builder->getQuery()->joins;

        if ($joins == null) {
            return false;
        }

        foreach ($joins as $join) {
            if ($join->table == $table) {
                return true;
            }
        }

        return false;
    }
}
