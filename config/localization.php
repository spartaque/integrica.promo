<?php
declare(strict_types=1);

return [
    'default_locale' => config('app.fallback_locale'),

    'supported_locales' => [
        'uk' => [
            'native' => 'Українська',
            'regional_code' => 'uk_UA',
            'charset' => 'UTF-8',
            'constants' => ['LC_TIME'],
        ],
        'ru' => [
            'native' => 'Русский',
            'regional_code' => 'ru_RU',
            'charset' => 'UTF-8',
            'constants' => ['LC_TIME'],
        ],
    ],

    'hide_default_locale_in_url' => true,
];
