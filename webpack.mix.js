const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    // .sass('resources/promo/styles/login.scss', 'public/dist/promo/styles/login.css')
    // .js('resources/js/promo_app.js', 'public/dist/js/login.js')
    .sass('resources/sass/app.scss', 'public/dist/styles/promo.css')
    .scripts('resources/js/libraries.js', 'public/dist/js/libraries.js')
    .scripts('resources/js/app.js', 'public/dist/js/app.js')
    .copy('resources/img', 'public/dist/img', false);
