// placeholder
$('input, textarea').each(function () {
    var placeholder = $(this).attr('placeholder');
    $(this).focus(function () {
        $(this).attr('placeholder', '');
    });
    $(this).focusout(function () {
        $(this).attr('placeholder', placeholder);
    });
});

// input mask
(function () {
    var input = $('.js-input-mask');
    if (input.length) {
        input.each(function () {
            var _this = $(this),
                mask = _this.data('mask');
            _this.mask(mask);
        });
    }
})();

// get scrollbar width
function getScrollBarWidth() {
    var outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
        widthWithScroll = $('<div>').css({width: '100%'}).appendTo(outer).outerWidth();
    outer.remove();
    return 100 - widthWithScroll;
};

// showPopup
function showPopup(el) {
    var body = $('body');
    body.addClass('no-scroll');
    body.css('padding-right', getScrollBarWidth());
    if (el) {
        el.addClass('animation');
        setTimeout(function () {
            el.addClass('visible');
        }, 10);
    }
}

// popup
(function () {
    var body = $('body'),
        wrap = $('.js-popup-wrap'),
        close = $('.js-popup-close');

    var el = void 0;
    $('[data-popup]').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var _this = $(this),
            data = _this.data('popup');
        el = $(data);
        showPopup(el);
    });
    close.on('click', function (e) {
        var _thisClose = $(this);
        el = _thisClose.parents('.js-popup');
        e.preventDefault();
        hidePopup();
    });
    wrap.on('click', function (e) {
        e.stopPropagation();
    });
    $('.popup').on('click', function () {
        var _thisPopup = $(this);
        el = _thisPopup;
        hidePopup();
    });
    $(document).keyup(function (e) {
        if (e.keyCode === 27) hidePopup();
    });

    function hidePopup() {
        if (el) {
            el.removeClass('animation');
            if ($('.js-popup.visible').length == 1) {
                body.removeClass('no-scroll');
                body.css('padding-right', 0);
            }
            setTimeout(function () {
                el.removeClass('visible');
            }, 500);
        }
    }
})();

// animate
$(document).ready(function () {
    // return;
    InitWaypointAnimations({
        delay: '0s',
        offset: '35%',
        animateClass: 'ez-animate',
        animateGroupClass: 'ez-animate-group',
    });

    $('#months, #employees').change(function () {
        var employees = parseInt($('#employees').val()),
            months = parseInt($('#months').val());

        $.ajax({
            url: '/api/license/getPrice?employees=' + employees + '&months=' + months,
            type: 'GET',
            contentType: false,
            cache: false,
            processData: false,
            success: function success(price) {
                $('#price-value').text(price);
            },
        });
    });
});

// validate
(function () {
    $('.js-form').each(function () {
        $(this).validate({
            errorPlacement: function errorPlacement(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error);
                } else {
                    error.insertAfter(element.parent());
                }
            },
            submitHandler: function submitHandler(form) {
                var submit_btn = $('[type=submit]', form).first();

                submit_btn.attr('disabled', true);

                var formData = new FormData(form);

                formData.append('grecaptcha_token', grecaptcha.getResponse());

                $('.js-loading').show();

                $.ajax({
                    url: '/api/companies/consultation',
                    data: formData,
                    type: 'POST',
                    // dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function success(data) {
                        if (typeof data.redirect_url === 'undefined') {
                            showPopup($('.js-popup-thank'));
                        } else {
                            setTimeout(function () {
                                location.href = data.redirect_url;
                            }, 1000);
                        }

                        fbq('track', 'Lead');
                        gtag_report_conversion();
                    },
                    error: function (error) {
                        var errors = '';
                        for (var field_name in error.responseJSON.errors) {
                            for (var item in error.responseJSON.errors[field_name]) {
                                errors += error.responseJSON.errors[field_name][item] + '<br>';
                            }
                        }

                        $(form).find('.field').last().append('<label class="error">' + errors + '</label>');
                    },
                    complete: function () {
                        submit_btn.attr('disabled', false);
                    },
                });
            },
        });
    });
})();

// scroll down
$('.js-arrow').click(function () {
    $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top + 'px',
    }, {
        duration: 1000,
    });
    return false;
});

function scroll() {
    var scrollTop = $(window).scrollTop(),
        header = $('.js-header');
    if (scrollTop > 30) {
        header.addClass('active');
    } else {
        header.removeClass('active');
    }
}

scroll();

$(window).scroll(function () {
    scroll();
});

// header
(function () {
    var header = $('.js-header'),
        burger = $('.js-header-burger'),
        wrap = $('.js-header-wrap'),
        bg = $('.js-header-bg'),
        link = $('.js-header-link'),
        html = $('html'),
        body = $('body');
    burger.on('click', function () {
        burger.toggleClass('active');
        wrap.toggleClass('visible');
        html.toggleClass('no-scroll');
        body.toggleClass('no-scroll');
        bg.toggleClass('show');
    });
    bg.on('click', function () {
        burger.removeClass('active');
        wrap.removeClass('visible');
        html.removeClass('no-scroll');
        body.removeClass('no-scroll');
        bg.removeClass('show');
    });
    link.on('click', function (e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
        burger.removeClass('active');
        wrap.removeClass('visible');
        html.removeClass('no-scroll');
        body.removeClass('no-scroll');
        bg.removeClass('show');
    });
})();

(function () {
    var windowWidth = $(window).width();
    if (windowWidth >= 1023) {
        $('#picture-parallax').parallax();
    }
})();

$(document).ready(function () {
    $(window).scroll(function (e) {
        parallaxScroll();
    });

    function parallaxScroll() {
        var scrolled = $(window).scrollTop(),
            preview = $('.preview');
        var windowWidth = $(window).width();
        if (windowWidth >= 1279) {
            preview.css('transform', 'translateY(' + scrolled * .15 + 'px)');
        }
    }
});
