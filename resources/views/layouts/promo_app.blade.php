<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>{{ trans('promo.title') }}</title>
    @include('parts.favicon')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="keywords" content="{{ trans('promo.meta_keywords') }}">
    <meta name="description" content="{{ trans('promo.meta_description') }}">
    <link rel="alternate" href="{{ config('app.url')  }}" hreflang="uk"/>
    @include('index.parts.header.og_tags')

    @yield('styles')

    <link
        href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,900&display=swap&subset=cyrillic"
        rel="stylesheet"
        media="all"
    >
    <link href="/css/app.css?{{ Config::get('env.APP_VERSION') }}" rel="stylesheet" media="all">

    @include('parts.viewport')
</head>
<body>
<div class="out">
    @include('index.parts.header', ['route' => $route ?? ''])

    @yield('content')

    @include('index.parts.popup')
</div>

@include('index.parts.global_variables')
@include('index.parts.referral')
@include('parts.i18n')

@yield('scripts')

<script src="/js/libraries.js"></script>
<script src="/js/app.js"></script>

</body>
</html>
