@extends('promo.metronic_app',[
'title'=> trans('messages.contacts'),
'translations' => ['promo'],
])

@section('content')
  <div class="m-grid__item m-grid__item--fluid  m-grid__item--order-tablet-and-mobile-1  m-login__wrapper">
    <h4>Контакты для связи</h4><br>
    <h5>Email: support@integrica.com.ua</h5><br>
  </div>
@endsection
