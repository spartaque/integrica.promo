<div class="program">
  <div class="program__anchor anchor" id="program"></div>
  <div class="program__center center">
    <h2 class="title">{!! trans('promo.program.1') !!}</h2>
    <div class="program__container">
      <div class="program__row">
        <div class="program__col">
          <div class="program__img">
            <img class="program__pic" src="/img/program-pic-1.svg" alt="" width="586">
          </div>
        </div>
        <div class="program__col">
          <div class="program__category">{{ trans('promo.program.2') }}</div>
          <div class="program__body">
            {!!  trans('promo.program.3') !!}
            <br>
            <br>
            {{ trans('promo.program.4') }}
          </div>
        </div>
      </div>
      <div class="program__row">
        <div class="program__col">
          <div class="program__category">{{ trans('promo.program.5') }}</div>
          <div class="program__body">
            {!!  trans('promo.program.6') !!}
            <br>
            <br>
            {{ trans('promo.program.7') }}
          </div>
        </div>
        <div class="program__col">
          <div class="program__img">
            <img class="program__pic" src="/img/program-pic-2.png" alt="" width="568">
          </div>
        </div>
      </div>
      <div class="program__row">
        <div class="program__col">
          <div class="program__img">
            <img class="program__pic" src="/img/program-pic-3.png" alt="" width="532">
          </div>
        </div>
        <div class="program__col">
          <div class="program__category">{{ trans('promo.program.8') }}</div>
          <div class="program__body">
            {!!  trans('promo.program.9') !!}
            <br>
            <br>
            {{ trans('promo.program.10') }}
          </div>
        </div>
      </div>
      <div class="program__row">
        <div class="program__col">
          <div class="program__category">{{ trans('promo.program.11') }}</div>
          <div class="program__body">
            {!!  trans('promo.program.12') !!}
            <br>
            <br>
            {{ trans('promo.program.13') }}
          </div>
        </div>
        <div class="program__col">
          <div class="program__img">
            <img class="program__pic" src="/img/system-pic.svg" alt="" width="568">
          </div>
        </div>
      </div>
      <div class="program__row">
        <div class="program__col">
          <div class="program__img">
            <img class="program__pic" src="/img/program-pic-3.png" alt="" width="532">
          </div>
        </div>
        <div class="program__col">
          <div class="program__category">{{ trans('promo.program.14') }}</div>
          <div class="program__body">
            {!!  trans('promo.program.15') !!}
            <br>
            <br>
            {{ trans('promo.program.16') }}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="program__bg">
    <div class="program__preview preview"><img class="program__pic" src="/img/bg-6.png" alt=""></div>
    <div class="program__preview preview"><img class="program__pic" src="/img/bg-11.png" alt=""></div>
    <div class="program__preview preview"><img class="program__pic" src="/img/bg-8.png" alt=""></div>
    <div class="program__preview preview"><img class="program__pic" src="/img/bg-7.png" alt=""></div>
    <div class="program__preview preview"><img class="program__pic" src="/img/bg-11.png" alt=""></div>
  </div>
</div>
