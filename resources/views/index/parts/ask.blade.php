<div class="page ask">
  <div class="anchor" id="ask"></div>
  <div class="ask__center center">
    <h2 class="title title_line">{{ trans('promo.aks.1') }}</h2>
    <div class="info">{{ trans('promo.aks.2') }}</div>
    <div class="ask__row">
      <form class="ask__form js-form">
        <div class="ask__fieldset">
          <div class="ask__field field">
            <div class="field__wrap">
              <input class="field__input" name="fio" required placeholder="{{ trans('promo.aks.3') }}">
            </div>
          </div>
          <div class="ask__field field">
            <div class="field__wrap">
              <input
                      onfocus="initRecaptcha()"
                      class="field__input js-input-mask"
                      name="phone"
                      placeholder="{{ trans('promo.aks.4') }}"
                      data-mask="{{ trans('promo.aks.5') }}"
                      required
              >
            </div>
          </div>
          <div class="ask__field field">
            <div class="field__wrap">
              <input class="field__input" type="email" name="email" required placeholder="Email">
            </div>
          </div>
          <div class="ask__field field">
            <div class="g-recaptcha" data-sitekey="{{ config('env.GOOGLE_RECAPTCHA_KEY') }}"></div>
          </div>
        </div>
        <div class="ask__btns">
          <button class="ask__btn btn" type="submit">{{ trans('promo.aks.6') }}</button>
        </div>
        <div class="ask__img"><img class="ask__pic" src="/img/bg-ask.svg" alt=""></div>
      </form>
      <div class="ask__contacts">
        <div class="ask__text">{{ trans('promo.aks.7') }}</div>
        <div class="ask__text">
          <div class="ask__item">
            <a
                    class="ask__link"
                    href="viber://chat?number={{ config('app.phone') }}"
                    style="display: flex; align-items: center;margin-bottom: 10px"
            ><img src="/images/viber.png" alt="Viber" width="24">
              <div style="margin-left: 10px">Viber</div>
            </a>
            <a
                    class="ask__link"
                    href="tg://resolve?domain={{ config('app.telegram') }}"
                    style="display: flex; align-items: center;margin-bottom: 10px"
            ><img src="/images/telegram.png" alt="Telegram" width="24">
              <div style="margin-left: 10px">Telegram</div>
            </a>
            <a
                    class="ask__link"
                    href="http://m.me/integrica.crm"
                    target="_blank"
                    rel="nofollow noopener"
                    style="display: flex; align-items: center"
            ><img src="/images/messenger.png" alt="Messenger" width="24">
              <div style="margin-left: 10px">Messenger</div>
            </a>
          </div>
        </div>
        <div class="ask__text">{{ trans('promo.aks.8') }}</div>
        <div class="ask__social social">
          {{--            <a class="social__link" href="#">--}}
          {{--              <svg class="icon icon-youtube">--}}
          {{--                <use xlink:href="/img/sprite.svg#icon-youtube"></use>--}}
          {{--              </svg>--}}
          {{--            </a>--}}
          <a
                  class="social__link"
                  href="https://www.facebook.com/Integrica-система-управления-салоном-красоты-611794762669269"
                  target="_blank"
                  rel="nofollow noopener"
          >
            <svg class="icon icon-facebook">
              <use xlink:href="/img/sprite.svg#icon-facebook"></use>
            </svg>
          </a>
          {{--          <a class="social__link" href="viber://chat?number={{ config('app.phone') }}">--}}
          {{--            <svg class="icon icon-viber">--}}
          {{--              <use xlink:href="/img/sprite.svg#icon-viber"></use>--}}
          {{--            </svg>--}}
          {{--          </a>--}}
          {{--          <a class="social__link" href="tg://resolve?domain=alexander_integrica">--}}
          {{--            <svg class="icon icon-telegram">--}}
          {{--              <use xlink:href="/img/sprite.svg#icon-telegram"></use>--}}
          {{--            </svg>--}}
          {{--          </a>--}}
          <a class="social__link" href="https://instagram.com/integrica_crm/" target="_blank" rel="nofollow noopener">
            <svg class="icon icon-instagram">
              <use xlink:href="/img/sprite.svg#icon-instagram"></use>
            </svg>
          </a>
        </div>
        <div class="ask__text">
          <img class="va__baseline" src="/images/ukraine.png" alt="Ukraine" width="16">
          {{ trans('promo.address') }}
        </div>
      </div>
      <div class="ask__preview"><img class="ask__pic" src="/img/ask-pic.png" alt=""></div>
    </div>
  </div>
  <div class="footer">
    <div class="footer__center center">
      <div class="footer__copyright">{{ trans('promo.aks.9') }}</div>
    </div>
  </div>
</div>

@section('scripts')
  <script>
      App.recaptcha_loaded = false;

      function initRecaptcha() {
          if (App.recaptcha_loaded) {
              return;
          }

          var file = document.createElement('script');
          file.setAttribute('type', 'text/javascript');
          file.setAttribute('src', 'https://www.google.com/recaptcha/api.js');

          document.getElementsByTagName('head')[0].appendChild(file);

          App.recaptcha_loaded = true;
      }
  </script>
@endsection
