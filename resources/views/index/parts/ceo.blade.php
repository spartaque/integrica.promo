<div class="ceo gradient-page">
  <div class="anchor" id="ceo"></div>
  <div class="center">
    <h2 class="ceo__title title">{!! trans('promo.ceo.1') !!}</h2>
    <div class="program__container">
      <div class="program__row">
        <div class="program__col">
          <div class="program__img">
            <img class="program__pic ceo__pic" src="/img/ceo.png" alt="CEO" width="auto">
          </div>
        </div>
        <div class="program__col">
          {!! trans('promo.ceo.2') !!}
          <br>
          <br>
          {!! trans('promo.ceo.3') !!}
          <br>
          <br>

          <div class="ceo__info">
            <b>
              {!! trans('promo.ceo.4') !!}
            </b>
            <div class="ceo-social">
              <a href="https://www.facebook.com/tychko.oleksandr" target="_blank" rel="nofollow noopener">
                <img src="/img/facebook.png" alt="" width="24">
              </a>
              <a href="https://www.instagram.com/tychko_oleksandr/" target="_blank" rel="nofollow noopener">
                <img src="/img/instagram.png" alt="" width="24">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="gradient-page__bottom"></div>
  </div>
</div>
