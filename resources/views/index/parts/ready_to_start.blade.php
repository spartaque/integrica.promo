<div class="page ready_to_start">
  <div class="anchor" id="ready_to_start"></div>
  <div class="ready_to_start__center center">
    <div class="ready_to_start__top">
      <h2 class="ready_to_start__title title">{{ trans('promo.ready_to_start.1') }}</h2>
      <div class="ready_to_start__list ez-animate-group">
        <div class="ready_to_start__item ez-animate" data-animation="fadeIn">
          <div class="ready_to_start__icon">
            <img class="ready_to_start__pic" src="/images/emoji/thinking.png" alt="" width="49">
          </div>
          <div class="ready_to_start__text">
            <div class="step">1</div>
            {!! trans('promo.ready_to_start.2') !!}
{{--            <a href="{{ route(locale() .'.register')  }}" target="_blank" rel="nofollow noopener">--}}
            <a href="/register" target="_blank" rel="nofollow noopener">
              {!! trans('promo.ready_to_start.3') !!}
            </a>
            {!! trans('promo.ready_to_start.4') !!}
          </div>
        </div>
        <div class="ready_to_start__item ez-animate" data-animation="fadeIn" data-animation-delay="0.25s">
          <div class="ready_to_start__icon">
            <img class="ready_to_start__pic" src="/images/emoji/female-technologist.png" alt="" width="72">
          </div>
          <div class="ready_to_start__text">
            <div class="step">2</div>
            {!! trans('promo.ready_to_start.5') !!}
          </div>
        </div>
        <div class="ready_to_start__item ez-animate" data-animation="fadeIn" data-animation-delay="0.5s">
          <div class="ready_to_start__icon">
            <img class="ready_to_start__pic" src="/images/emoji/sunglasses.png" alt="" width="60">
          </div>
          <div class="ready_to_start__text">
            <div class="step">3</div>
            {!! trans('promo.ready_to_start.6') !!}
          </div>
        </div>
      </div>
    </div>
    <div class="ready_to_start__info">
      <div class="order__row">
        <div class="order__fieldset" style="justify-content: center">
          <div class="field__wrap">
{{--            <a href="{{ route(locale() . '.register')  }}" class="order__btn btn" type="submit">--}}
            <a href="/register" class="order__btn btn" type="submit">
              {{ trans('promo.sign_up') }}!
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
