<script>
    API_HOST = '{{ config('env.API_HOST') }}';
    APP_ENV = '{{ config('app.env') }}';
    SENTRY_DSN = '{{ config('sentry.dsn') }}';
    GOOGLE_RECAPTCHA_KEY = '{{ config('env.GOOGLE_RECAPTCHA_KEY') }}';
    App = {
{{--        routes: {!! json_encode(getPromoRoutes()) !!},--}}
        trans: {
            validation: {
                required: '{{ trans('promo.validation.required') }}',
                email: '{{ trans('promo.validation.email') }}',
            },
        },
    };
</script>
