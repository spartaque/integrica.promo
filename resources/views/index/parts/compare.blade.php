<div class="page compare">
  <div class="anchor" id="compare"></div>
  <div class="center">
    <h2 class="ceo__title title title_line">{!! trans('promo.compare.title') !!}</h2>
    <div class="system__info info">{{ trans('promo.compare.subtitle') }}</div>
    <div class="program__container" style="overflow-x: auto">
      <table class="table compare-table table-striped">
        <thead>
        <tr>
          <th></th>
          <th>Integrica</th>
          <th>YCLIENTS</th>
          <th>Beauty Pro</th>
          <th>Appointer</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>{{ trans('promo.compare.2') }}</td>
          <td>{{ trans('promo.compare.2_1') }}</td>
          <td>{{ trans('promo.compare.2_2') }}</td>
          <td>{{ trans('promo.compare.2_2') }}</td>
          <td>{{ trans('promo.compare.2_4') }}</td>
        </tr>
        <tr>
          <td>{{ trans('promo.compare.3') }}</td>
          <td>{{ trans('promo.compare.3_1') }}</td>
          <td>{{ trans('promo.compare.3_1') }}</td>
          <td>{{ trans('promo.compare.3_2') }}</td>
          <td>{{ trans('promo.compare.3_1') }}</td>
        </tr>
        <tr>
          <td>
            {{ trans('promo.compare.4') }}
          </td>
          <td>
            <img src="/images/ukraine.png" alt="Ukraine" width="16">
            {{ trans('promo.compare.4_1') }}
          </td>
          <td>
            <img src="/images/russia.png" alt="Russia" width="16">
            {{ trans('promo.compare.4_2') }}
          </td>
          <td>
            <img src="/images/ukraine.png" alt="Ukraine" width="16">
            {{ trans('promo.compare.4_1') }}
          </td>
          <td>
            <img src="/images/ukraine.png" alt="Ukraine" width="16">
            {{ trans('promo.compare.4_1') }}
          </td>
        </tr>
        <tr>
          <td>{{ trans('promo.compare.5') }}</td>
          <td>{{ trans('promo.compare.5_1') }}</td>
          <td>{{ trans('promo.compare.5_2') }}</td>
          <td>{{ trans('promo.compare.5_3') }}</td>
          <td>{{ trans('promo.compare.5_4') }}</td>
        </tr>
        <tr>
          <td>{{ trans('promo.compare.6') }}</td>
          <td>{{ trans('promo.compare.6_1') }}</td>
          <td>{{ trans('promo.compare.6_2') }}</td>
          <td>{{ trans('promo.compare.6_3') }}</td>
          <td>{{ trans('promo.compare.6_4') }}</td>
        </tr>
        <tr>
          <td>{{ trans('promo.compare.7') }}</td>
          <td>
            {{ trans('promo.compare.7_1') }}
            <img src="/images/emoji/fire.png" alt="Fire" width="16">
          </td>
          <td>1</td>
          <td>1</td>
          <td>1</td>
        </tr>
        <tr>
          <td>{{ trans('promo.compare.1') }}</td>
          <td>{{ trans('promo.compare.1_1') }}</td>
          <td>{{ trans('promo.compare.1_2') }}</td>
          <td>{{ trans('promo.compare.1_2') }}</td>
          <td>{{ trans('promo.compare.1_1') }}</td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
