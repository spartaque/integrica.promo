<div class="quality">
  <div class="ask__anchor anchor" id="quality"></div>
  <div class="quality__center center">
    <div class="quality__top">
      <h2 class="quality__title title">{{ trans('promo.quality.1') }}</h2>
      <div class="quality__list ez-animate-group">
        {{--        <div class="quality__item ez-animate" data-animation="fadeIn">--}}
        {{--          <div class="quality__icon"><img class="quality__pic" src="/img/write-letter.svg" alt="" width="60">--}}
        {{--          </div>--}}
        {{--          <div class="quality__text">Все еще доверяете блокнотам <br>и журналам, в которых за день <br>возникает до 10--}}
        {{--            правок и <br>исправлений?--}}
        {{--          </div>--}}
        {{--        </div>--}}
        <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="0.25s">
          <div class="quality__icon">
            <img class="quality__pic" src="/img/woman.svg" alt="" width="49">
          </div>
          <div class="quality__text">
            {!! trans('promo.quality.2') !!}
          </div>
        </div>
        <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="0.5s">
          <div class="quality__icon">
            <img class="quality__pic" src="/img/bars.svg" alt="" width="72">
          </div>
          <div class="quality__text">
            {!! trans('promo.quality.3') !!}
          </div>
        </div>
        <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="0.75s">
          <div class="quality__icon">
            <img class="quality__pic" src="/img/powder.svg" alt="" width="60">
          </div>
          <div class="quality__text">
            {!! trans('promo.quality.4') !!}
          </div>
        </div>
        {{--          <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="1s">--}}
        {{--            <div class="quality__icon"><img class="quality__pic" src="/img/time-passing.svg" alt="" width="60">--}}
        {{--            </div>--}}
        {{--            <div class="quality__text">Разговоры с клиентами длятся <br>более 10 минут рабочего времени, <br>а все--}}
        {{--              потому что непонятно, <br>где и когда есть свободное <br>окно в графике--}}
        {{--            </div>--}}
        {{--          </div>--}}
        {{--        <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="1.25s">--}}
        {{--          <div class="quality__icon"><img class="quality__pic" src="/img/money.svg" alt="" width="61"></div>--}}
        {{--          <div class="quality__text">Расчет заработной платы занимает много <br>времени и у вас постоянно в <br>бумажных--}}
        {{--            документах <br>возникает путаница--}}
        {{--          </div>--}}
        {{--        </div>--}}
        {{--          <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="1.5s">--}}
        {{--            <div class="quality__icon"><img class="quality__pic" src="/img/ask.svg" alt="" width="52"></div>--}}
        {{--            <div class="quality__text">Вам сложно контактировать <br>с клиентами, потому что о <br>них абсолютно ничего--}}
        {{--              <br>не известно.--}}
        {{--            </div>--}}
        {{--          </div>--}}
      </div>
    </div>
    <div class="quality__bottom">
      <h2 class="quality__title title">{{ trans('promo.quality.5') }}</h2>
      <div class="quality__list ez-animate-group">
        <div class="quality__item ez-animate" data-animation="fadeIn">
          <div class="quality__icon">
            <img class="quality__pic" src="/img/responsive.svg" alt="" width="70">
          </div>
          <div class="quality__text">
            {!! trans('promo.quality.6') !!}
          </div>
        </div>
        {{--        <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="0.25s">--}}
        {{--          <div class="quality__icon"><img class="quality__pic" src="/img/chat.svg" alt="" width="74"></div>--}}
        {{--          <div class="quality__text">Управляйте <br>бизнес-процессами, <br>где бы вы не находились</div>--}}
        {{--        </div>--}}
        <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="0.5s">
          <div class="quality__icon">
            <img class="quality__pic" src="/img/id-card.svg" alt="" width="43">
          </div>
          <div class="quality__text">
            {!! trans('promo.quality.7') !!}
          </div>
        </div>
        {{--          <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="0.75s">--}}
        {{--            <div class="quality__icon"><img class="quality__pic" src="/img/test.svg" alt="" width="61"></div>--}}
        {{--            <div class="quality__text">Записывайте клиентов <br>на точное время без <br>возможных ошибок</div>--}}
        {{--          </div>--}}
        {{--          <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="1s">--}}
        {{--            <div class="quality__icon"><img class="quality__pic" src="/img/calendar.svg" alt="" width="61"></div>--}}
        {{--            <div class="quality__text">Следите за каждым <br>изменением в графике <br>в любое время</div>--}}
        {{--          </div>--}}
        <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="1.25s">
          <div class="quality__icon">
            <img class="quality__pic" src="/img/exchange.svg" alt="" width="57">
          </div>
          <div class="quality__text">
            {!! trans('promo.quality.8') !!}
          </div>
        </div>
        {{--        <div class="quality__item ez-animate" data-animation="fadeIn" data-animation-delay="1.5s">--}}
        {{--          <div class="quality__icon"><img class="quality__pic" src="/img/happy.svg" alt="" width="54"></div>--}}
        {{--          <div class="quality__text">С помощью программы <br>улучшите условия труда <br>для своих мастеров</div>--}}
        {{--        </div>--}}
      </div>
    </div>
    <div class="quality__info">
      {!! trans('promo.quality.9') !!}
      {!! trans('promo.quality.10') !!}
    </div>
  </div>
  <div class="quality__bg">
    <div class="quality__preview preview"><img class="quality__pic" src="/img/bg-1.png" alt=""></div>
    <div class="quality__preview preview"><img class="quality__pic" src="/img/bg-2.png" alt=""></div>
    <div class="quality__preview preview"><img class="quality__pic" src="/img/bg-3.png" alt=""></div>
    <div class="quality__preview preview"><img class="quality__pic" src="/img/bg-4.png" alt=""></div>
  </div>
</div>
