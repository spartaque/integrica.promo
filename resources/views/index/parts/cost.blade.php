<div class="page cost">
  <div class="cost__anchor anchor" id="cost"></div>
  <div class="cost__center center">
    <h2 class="cost__title title title_line">{!! trans('promo.cost.1') !!}</h2>
    <div class="cost__info info">
      {{ trans('promo.cost.2') }} <span class="free-price">{{ trans('promo.cost.3') }}</span>
    </div>
    <div class="cost__list">
      <div class="cost__item">
        <div class="cost__icon">
          <img class="cost__pic" src="/images/emoji/cat.png" alt="" width="46">
        </div>
        <div class="cost__category">{{ trans('promo.cost.4') }}</div>
        <div class="cost__text">
          <ul>
            <li>- {{ trans('promo.cost.free_1') }}</li>
            <li>- {{ trans('promo.cost.free_2') }}</li>
            <li>- {{ trans('promo.cost.free_3') }}</li>
            <li>- {{ trans('promo.cost.free_4') }}</li>
          </ul>
        </div>
      </div>
      <div class="cost__item">
        <div class="cost__icon">
          <img class="cost__pic" src="/images/emoji/sunglasses.png" alt="" width="60">
        </div>
        <div class="cost__category">{{ trans('promo.cost.5') }}</div>
        <div class="cost__text">

          <div class="form-group">
            <label for="employees">{{ trans('promo.cost.employees') }}</label>
            <select class="form-control" id="employees">
              <option value="5">{{ trans('promo.cost.to_5') }}</option>
              <option value="10">{{ trans('promo.cost.to_10') }}</option>
              <option value="20">{{ trans('promo.cost.to_20') }}</option>
              <option value="-1">{{ trans('promo.cost.to_-1') }}</option>
            </select>
          </div>
          <div class="form-group">
            <label for="employees">{{ trans('promo.cost.months') }}</label>
            <select class="form-control" id="months">
              <option value="1" selected>{{ trans('promo.cost.to_1_month') }}</option>
              <option value="3">{{ trans('promo.cost.to_3_months') }}</option>
              <option value="12">{{ trans('promo.cost.to_1_year') }}</option>
              <option value="24">{{ trans('promo.cost.to_2_years') }}</option>
            </select>
          </div>

          <div class="form-group price-container">
            <h1 id="price-value">250</h1>
            <div class="price-currency"> {{ trans('promo.cost.uah') }}</div>
          </div>
        </div>
      </div>
    </div>
    <div class="cost__info info cost__info_bottom">
      {{ trans('promo.cost.7')  }}
      <br>
      <b>{{ trans('promo.cost.8')  }}</b>
      {{ trans('promo.cost.9')  }}
    </div>
    <div class="order__row">
      <div class="order__fieldset" style="justify-content: center">
        <div class="field__wrap">
{{--          <a href="{{ route(locale() . '.register')  }}" class="order__btn btn" type="submit">--}}
          <a href="/register" class="order__btn btn" type="submit">
            {{ trans('promo.sign_up') }}!
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="cost__bg">
    <div class="cost__preview preview"><img class="cost__pic" src="/img/bg-10.png" alt=""></div>
    <div class="cost__preview preview"><img class="cost__pic" src="/img/bg-11.png" alt=""></div>
  </div>
</div>
