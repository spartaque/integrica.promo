{{--<!--Open Graph data-->--}}
<meta property="og:title" content="{{ trans('promo.title') }}"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="https://integrica.com.ua"/>
<meta property="og:image" content="https://integrica.com.ua/images/logo_square.jpg"/>
<meta property="og:site_name" content="Integrica"/>
{{--<meta property="og:description" content="Короткое описание для анонса."/>--}}

{{--  <!--Twitter Card data-->--}}
{{--  <meta name="twitter:card" content="summary">--}}
{{--  <meta name="twitter:site" content="@publisher_handle">--}}
{{--  <meta name="twitter:title" content="Page Title">--}}
{{--  <meta name="twitter:description" content="Page description less than 200 characters">--}}
{{--  <meta name="twitter:creator" content="@author_handle">--}}
{{--  <meta name="twitter:image" content="http://www.example.com/image.jpg">--}}

{{-- Для Вконтакте и прочих http://kharchuk.ru/home/9-%D0%9F%D1%80%D0%BE%D1%87%D0%B5%D0%B5/135-social-meta-tags?template=yougrids --}}
<link rel="image_src" href="{{ isset($image_src) ? $image_src : '/images/logo_square.jpg' }}"/>
