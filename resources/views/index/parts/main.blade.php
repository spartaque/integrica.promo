<div class="main" id="main">
  <div class="main__center center">
    <div class="main__wrap">
      <h1 class="main__title">
          <span class="main__box">
            <span class="main__line">
              <strong>{!! trans('promo.main_title') !!}</strong></span>
          </span>
        <span class="main__box">
            <span class="main__line">{{ trans('promo.main_1') }}</span>
          </span>
        <span class="main__box">
            <span class="main__line">{{ trans('promo.main_2') }}</span>
          </span>
        <span class="main__box">
            <span class="main__line">{{ trans('promo.main_3') }}</span>
        </span>
      </h1>
      <div class="main__btn_container">
{{--        <a class="main__btn btn js-arrow" href="{{ route(locale() .'.register') }}">--}}
        <a class="main__btn btn js-arrow" href="/register">
          {{ trans('promo.sign_up') }}
        </a>
        <div class="main__or">{{ trans('promo.or')  }}</div>
        <a class="more__btn btn js-arrow" href="#{{ $main_btn ?? 'ceo' }}">
          {{ trans('promo.details') }}
        </a>
      </div>
    </div>
    <div class="main__preview" id="picture-parallax">
      <div class="main__layer layer" data-depth=".8">
        <img class="main__pic" src="/img/main-pic.png" alt="">
      </div>
    </div>
  </div>
</div>
