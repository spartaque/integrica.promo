@php
  $slash = '/';
  if(isset($route) && \Illuminate\Support\Str::endsWith($route, 'index')){
        $slash = '';
  }
@endphp

<header class="header js-header">
  <div class="header__center center">
{{--    <a class="header__logo" href="{{ route(locale() .'.index')  }}">--}}
    <a class="header__logo" href="/">
      <img class="header__pic" src="/img/logo_new.png" alt="Logo">
    </a>
    <div class="header__wrap js-header-wrap">
      <nav class="header__nav">
        <a
                class="header__link @if(empty($slash)) js-header-link @endif"
                href="{{ $slash }}@if($slash && !l10n()->shouldHideLocaleInUrl(locale())){{ locale() }}@endif#main"
        >{{ trans('promo.main') }}
        </a>
        <a
                class="header__link @if(empty($slash)) js-header-link @endif"
                href="{{ $slash }}@if($slash && !l10n()->shouldHideLocaleInUrl(locale())){{ locale() }}@endif#program"
        >{{ trans('promo.opportunities') }}
        </a>
        <a
                class="header__link @if(empty($slash)) js-header-link @endif"
                href="{{ $slash }}@if($slash && !l10n()->shouldHideLocaleInUrl(locale())){{ locale() }}@endif#cost"
        >{{ trans('promo.prices') }}
        </a>
        <a
                class="header__link @if(empty($slash)) js-header-link @endif"
                href="{{ $slash }}@if($slash && !l10n()->shouldHideLocaleInUrl(locale())){{ locale() }}@endif#ask"
        >{{ trans('promo.contacts') }}
        </a>
      </nav>
      {{--      @switch(locale())--}}
      {{--        @case('ru')--}}
      {{--        <a class="header__entry header__text" href="{{ route('uk.index') }}">--}}
      {{--          UK--}}
      {{--        </a>--}}
      {{--        @break--}}
      {{--        @case('uk')--}}
      {{--        <a class="header__entry header__text" href="{{ route('ru.index') }}">--}}
      {{--          RU--}}
      {{--        </a>--}}
      {{--        @break--}}
      {{--      @endswitch--}}
{{--      <a class="header__entry" href="{{ route(locale() .'.login') }}">--}}
      <a class="header__entry" href="/login">
        <svg class="icon icon-login">
          <use xlink:hreheaderf="/img/sprite.svg#icon-login"></use>
        </svg>
        <div class="header__text">{{ trans('promo.login') }}</div>
      </a>
{{--      <a class="header__btn btn" href="{{ route(locale() .'.register')}}">--}}
      <a class="header__btn btn" href="/register">
        {{--          <svg class="icon icon-edit">--}}
        {{--            <use xlink:href="/img/sprite.svg#icon-edit"></use>--}}
        {{--          </svg>--}}
        <span class="btn__text">{{ trans('promo.sign_up') }}</span>
      </a>
      {{--      <a class="header__phone" href="tel:{{ config('app.phone') }}">{{ config('app.phone_formatted') }}</a>--}}
      {{--      <a class="header__mail" href="mailto:support@integrica.com.ua">{{ config('app.support_email') }}</a>--}}
      <div class="header__social social">
        {{--          <a class="social__link" href="#">--}}
        {{--            <svg class="icon icon-youtube">--}}
        {{--              <use xlink:href="/img/sprite.svg#icon-youtube"></use>--}}
        {{--            </svg>--}}
        {{--          </a>--}}
        <a class="social__link"
           href="https://www.facebook.com/Integrica-система-управления-салоном-красоты-611794762669269">
          <svg class="icon icon-facebook">
            <use xlink:href="/img/sprite.svg#icon-facebook"></use>
          </svg>
        </a>
        {{--          <a class="social__link" href="#">--}}
        {{--            <svg class="icon icon-viber">--}}
        {{--              <use xlink:href="/img/sprite.svg#icon-viber"></use>--}}
        {{--            </svg>--}}
        {{--          </a>--}}
        {{--          <a class="social__link" href="#">--}}
        {{--            <svg class="icon icon-telegram">--}}
        {{--              <use xlink:href="/img/sprite.svg#icon-telegram"></use>--}}
        {{--            </svg>--}}
        {{--          </a>--}}
        <a class="social__link" href="https://instagram.com/integrica_crm/">
          <svg class="icon icon-instagram">
            <use xlink:href="/img/sprite.svg#icon-instagram"></use>
          </svg>
        </a>
      </div>
    </div>
    <button class="header__burger burger js-header-burger"><span></span></button>
    <div class="header__bg js-header-bg"></div>
  </div>
</header>
