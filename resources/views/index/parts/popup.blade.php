<div class="popup popup_thank js-popup js-popup-thank">
  <div class="popup__wrap js-popup-wrap">
    <form class="popup__form js-form">
      <div class="popup__title">{{ trans('promo.popup.1') }}</div>
      <div class="popup__info">{{ trans('promo.popup.2') }}</div>
      <button class="popup__btn btn js-popup-close">{{ trans('promo.popup.3') }}</button>
    </form>
  </div>
</div>
