<div class="order order_consult">
  <div class="order__center center">
    <form class="order__form js-form">
      <h2 class="order__title title title_line">{{ trans('promo.application_title') }}</h2>
      <div class="order__info info">{{ trans('promo.application_subtitle') }}</div>
      <div class="order__row">
        <div class="order__fieldset" style="justify-content: center">
          <div class="order__field field">
            <div class="field__wrap">
              <a href="{{ route(locale() . '.register')  }}" class="order__btn btn" type="submit">
                {{ trans('promo.application') }}
              </a>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
