<div class="system">
  <div class="system__center center">
    <h2 class="system__title title title_line">{!! trans('promo.system.1') !!}</h2>
    <div class="system__info info">{!! trans('promo.system.2') !!}</div>
    <div class="system__row">
      <div class="system__col pt-0-sm">
        <ul class="system__list ez-animate-group">
          <li class="ez-animate" data-animation="fadeIn">
            {!! trans('promo.system.3') !!}
          </li>
          <li class="ez-animate" data-animation="fadeIn">
            {!! trans('promo.system.4') !!}
          </li>
          <li class="ez-animate" data-animation="fadeIn" data-animation-delay="0.2s">
            {!! trans('promo.system.5') !!}
          </li>
          <li class="ez-animate" data-animation="fadeIn" data-animation-delay="0.4s">
            {!! trans('promo.system.6') !!}
          </li>
          <li class="ez-animate" data-animation="fadeIn" data-animation-delay="0.6s">
            {!! trans('promo.system.7') !!}
          </li>
          <li class="ez-animate" data-animation="fadeIn" data-animation-delay="0.8s">
            {!! trans('promo.system.8') !!}
          </li>
          <li class="ez-animate" data-animation="fadeIn" data-animation-delay="1s">
            {!! trans('promo.system.9') !!}
          </li>
        </ul>
      </div>
      <div class="system__col hidden-sm">
        <div class="system__img"><img class="system__pic" src="/img/system-pic.svg" alt=""></div>
      </div>
    </div>
  </div>
  <div class="system__bg">
    <div class="system__preview preview"><img class="system__pic" src="/img/bg-3.png" alt=""></div>
    <div class="system__preview preview"><img class="system__pic" src="/img/bg-9.png" alt=""></div>
  </div>
</div>
