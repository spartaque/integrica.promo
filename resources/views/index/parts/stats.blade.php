<div class="page gradient-page stats">
  <div class="ask__anchor anchor" id="stats"></div>
  <div class="ready_to_start__center center">
    <div class="ready_to_start__top">
      <h2 class="ready_to_start__title title title_line">{{ trans('promo.stats.title') }}</h2>
      <div class="system__info info">{{ trans('promo.stats.subtitle') }}</div>
      <div class="ready_to_start__list ez-animate-group">
        <div class="stats__item ez-animate" data-animation="fadeIn">
          <div class="ready_to_start__icon">
            <img class="ready_to_start__pic" src="/images/emoji/heart.png" alt="" width="49">
          </div>
          <div class="ready_to_start__text">
            <h3>
              {!! trans('promo.stats.0') !!}
            </h3>
            {!! trans('promo.stats.0_1') !!}
          </div>
        </div>
        <div class="stats__item ez-animate" data-animation="fadeIn" data-animation-delay="0.25s">
          <div class="ready_to_start__icon">
            <img class="ready_to_start__pic" src="/images/emoji/sunglasses.png" alt="" width="49">
          </div>
          <div class="ready_to_start__text">
            <h3>
              {!! trans('promo.stats.1') !!}
            </h3>
            {!! trans('promo.stats.1_1') !!}
          </div>
        </div>
        <div class="stats__item ez-animate" data-animation="fadeIn" data-animation-delay="0.5s">
          <div class="ready_to_start__icon">
            <img class="ready_to_start__pic" src="/images/emoji/female-technologist.png" alt="" width="72">
          </div>
          <div class="ready_to_start__text">
            <h3>
              {!! trans('promo.stats.2') !!}
            </h3>
            {!! trans('promo.stats.2_1') !!}
          </div>
        </div>
        <div class="stats__item ez-animate" data-animation="fadeIn" data-animation-delay="0.75s">
          <div class="ready_to_start__icon">
            <img class="ready_to_start__pic" src="/images/emoji/walking_cat.png" alt="" width="60">
          </div>
          <div class="ready_to_start__text">
            <h3>
              {!! trans('promo.stats.3') !!}
            </h3>
            {!! trans('promo.stats.3_1') !!}
          </div>
        </div>
      </div>
    </div>
    <div class="ready_to_start__info">
      <div class="order__row">
        <div class="order__fieldset" style="justify-content: center">
          <div class="field__wrap">
{{--            <a href="{{ route(locale() . '.register')  }}" class="order__btn btn" type="submit">--}}
            <a href="/register" class="order__btn btn" type="submit">
              {{ trans('promo.sign_up') }}!
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="gradient-page__bottom"></div>
</div>
