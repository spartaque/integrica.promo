<script>
    var SESSION_DOMAIN = '{{  config('session.domain') }}';

    trackReferral();

    function trackReferral() {
        var has_referral_param = false;
        var referral = getReferral();

        if (referral !== undefined && referral !== '') {
            has_referral_param = true;
            setReferralCookie('r', referral);
        }

        var from_value = getUrlVars()['from'];
        if (from_value !== undefined && from_value !== '') {
            has_referral_param = true;
            setReferralCookie('from', from_value);
        }

        if (has_referral_param) {
            /*deleteReferralUrlParams();*/
        }

        setReferralCookie('referrer', document.referrer);
        setReferralCookie('location', location.href);
    }

    function getReferral() {
        if (location.hostname.startsWith('g.')) {
            return '9P5OQx2O';
        }

        return getUrlVars()['r'];
    }

    function deleteReferralUrlParams() {
        var params = getUrlParams(window.location.href);
        delete params.r;
        delete params.from;

        if (!Object.keys(params).length) {
            return;
        }

        var query_string = '?' + http_build_query(params);

        window.history.replaceState(params, null, query_string);
    }

    function setReferralCookie(param, value) {
        var two_week_seconds = 1209600;
        setCookie(param, value, {expires: two_week_seconds, domain: SESSION_DOMAIN});
    }

    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == 'number' && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + '=' + value;

        for (var propName in options) {
            updatedCookie += '; ' + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += '=' + propValue;
            }
        }

        document.cookie = updatedCookie;
    }

    function getCookie(name) {
        var matches = document.cookie.match(
            new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'),
        );
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function getUrlVars() {
        var vars = {};
        var href = decodeURIComponent(window.location.href);
        var answer_position = href.indexOf('?');
        var hash_position = href.indexOf('#');

        if (answer_position < 0 && hash_position > 0) {
            href = href.replace('#', '?');
            answer_position = hash_position;
        }
        if (answer_position > 0 && hash_position > 0) {
            href = href.replace('#', '&');
        }
        var hashes = href.slice(answer_position + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            var part = hashes[i].split('=');
            vars[part[0]] = part[1];
        }

        return vars;
    }

    function getUrlParams(url) {
        var params = {};
        var parser = document.createElement('a');
        parser.href = url;
        var query = parser.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            params[pair[0]] = decodeURIComponent(pair[1]);
        }
        return params;
    }

    function http_build_query(formdata, numeric_prefix, arg_separator, url_decode) {
        var value,
            key,
            tmp = [],
            that = this;

        if (url_decode === undefined) url_decode = true;

        var _http_build_query_helper = function (key, val, arg_separator) {
            var k,
                tmp = [];
            if (val === true) {
                val = '1';
            } else if (val === false) {
                val = '0';
            }
            if (val != null) {
                if (typeof val === 'object') {
                    for (k in val) {
                        if (val[k] != null) {
                            tmp.push(
                                _http_build_query_helper(key + '[' + k + ']', val[k], arg_separator),
                            );
                        }
                    }
                    return tmp.join(arg_separator);
                } else if (typeof val !== 'function') {
                    if (url_decode) {
                        return encodeURIComponent(key) + '=' + encodeURIComponent(val);
                    } else {
                        return key + '=' + val;
                    }
                } else {
                    throw new Error('There was an error processing for http_build_query().');
                }
            } else {
                return '';
            }
        };

        if (!arg_separator) {
            arg_separator = '&';
        }
        for (key in formdata) {
            value = formdata[key];
            if (numeric_prefix && !isNaN(key)) {
                key = String(numeric_prefix) + key;
            }
            var query = _http_build_query_helper(key, value, arg_separator);
            if (query !== '') {
                tmp.push(query);
            }
        }

        return tmp.join(arg_separator);
    }
</script>
