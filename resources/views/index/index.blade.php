@extends('layouts.promo_app',[
'title'=> trans('messages.contacts'),
'route' => Route::getCurrentRoute()->getName(),
'translations' => ['promo'],
])

@section('content')
  @include('index.parts.main')
  @include('index.parts.ceo')
  @include('index.parts.program')
  @include('index.parts.ready_to_start')
  {{--  @include('parts.system')--}}
  {{--  @include('parts.order_consult')--}}
  @include('index.parts.cost')
  @include('index.parts.compare')
  @include('index.parts.stats')
  @include('index.parts.ask')
@endsection
