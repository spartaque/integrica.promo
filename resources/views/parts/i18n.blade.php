<script>
  @php
    $translations = array_merge($translations ?? [], ['messages', 'common', 'events', 'products', 'help', 'currencies'])
  @endphp
      window.i18n = {
      locale: '{{ app()->getLocale() }}',
      fallbackLocale: '{{ config('app.fallback_locale') }}',
      messages: {!! getTranslations($translations ?? []) !!}
  };
</script>
